import argparse
from math import pi
import os
import h5py
import numpy as np
from computeR import *

parser = argparse.ArgumentParser(description='Find Optimal Stress Parameter given kinematic model and geometry')
parser.add_argument('filename', help='kinematic model filename (xdmf)')
args = parser.parse_args()

if not os.path.exists('data'):
    os.makedirs('data')

#read fault geometry and data from hdf5 file
prefix, ext = os.path.splitext(args.filename)

h5f = h5py.File(prefix+'.h5')
xyz = h5f['mesh0/geometry'][:,:]
connect = h5f['mesh0/connect'][:,:]
Sls = h5f['mesh0/Sls'][:]
Sld = h5f['mesh0/Sld'][:]
area = h5f['mesh0/area'][:]
slip=h5f['mesh0/ASl'][:]
rakeInv=np.arctan2(Sld,Sls)
h5f.close()

#compute various geometric object needed by Rcalc
zcenters,us,ud,un = ComputeZcUsUdUn(xyz, connect)

#just to get a smaller score
area0=np.average(area)
area=area/area0


Rtarg=0.6
mu_st=0.6
mu_dy=0.1
cohesion=0.0
rhoeff=890.
zref=-10e3
rake=0

from cParamComputeR import cParamComputeR
lstrike = np.arange(40,120,5)
ldip = np.arange(70,115,5)
#lrake = np.arange(-15,20,5)
lrake = np.array([0])
ls2norm = np.arange(15,55,5)

#lstrike = np.array([70])
#ldip =np.array([90])
#lrake = np.array([0])
#ls2norm = np.array([15])

#will contain the misfits of the grid search
lmis_rake = np.zeros((lstrike.shape[0],ldip.shape[0], lrake.shape[0], ls2norm.shape[0]))
lmis_slip = np.zeros((lstrike.shape[0],ldip.shape[0], lrake.shape[0], ls2norm.shape[0]))

#rake only compared where significant slip
idsslip = np.where(slip>2)
isslip=np.zeros(slip.shape)
isslip[idsslip]=1.0

for i,strike in enumerate(lstrike):
   for j,dip in enumerate(ldip):
      for k,rake in enumerate(lrake):
         for l,s2norm in enumerate(ls2norm):
            #load parameters
            par = cParamComputeR(Rtarg,mu_st,mu_dy,cohesion,rhoeff,zref,strike,dip,rake,s2norm)
            #compute R and rake
            (R, RakeCalc)= Rcalc(zcenters,us,ud,un, par)
            RakeCalc = -RakeCalc*pi/180.

            mis_rake = np.std(np.sqrt(area*isslip)*(RakeCalc-rakeInv)/(0.5*pi))
            mis_slip = np.std(np.sqrt(area*slip)*(Rtarg-R)/Rtarg)

            print par, mis_rake, mis_slip
            lmis_rake[i,j,k,l]=mis_rake
            lmis_slip[i,j,k,l]=mis_slip

#save data to postprocess it later
np.save('data/lmis_rake.npy', lmis_rake)
np.save('data/lmis_slip.npy', lmis_slip)
np.save('data/lstrike.npy', lstrike)
np.save('data/ldip.npy', ldip)
np.save('data/lrake.npy', lrake)
np.save('data/ls2norm.npy', ls2norm)


