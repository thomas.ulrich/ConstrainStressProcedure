class cParamComputeR:
   def __init__(self, Rcalc,mu_st,mu_dy,cohesion,rhoeff,zref,strike,dip,rake,s2norm):
      "Initialize parameters directly"
      self.Rcalc = Rcalc
      self.mu_st = mu_st
      self.mu_dy = mu_dy
      self.cohesion = cohesion
      self.rhoeff = rhoeff
      self.zref = zref
      self.strike = strike
      self.dip = dip
      self.rake = rake
      self.s2norm = s2norm/100.

   @classmethod
   def fromfilename(cls, ParFileName):
      "Initialize parameters from a file"
      import ConfigParser
      config = ConfigParser.ConfigParser()
      config.read(ParFileName)
      Rcalc = config.getfloat("computeRvars", "Rtarg")
      mu_st = config.getfloat("computeRvars", "mu_st")
      mu_dy = config.getfloat("computeRvars", "mu_dy")
      cohesion = config.getfloat("computeRvars", "cohesion")
      rhoeff = config.getfloat("computeRvars", "rhoeff")
      zref = config.getfloat("computeRvars", "zref")
      strike = config.getfloat("computeRvars", "strike")
      dip = config.getfloat("computeRvars", "dip")
      rake = config.getfloat("computeRvars", "rake")
      s2norm = config.getfloat("computeRvars", "s2norm")
      return cls(Rcalc,mu_st,mu_dy,cohesion,rhoeff,zref,strike,dip,rake,s2norm)

   def __repr__(self):
      return "<cParamComputeR: R:%.3f mus:%.3f mud:%.3f coh:%e rhoe:%.1f z:%e strike:%.1f dip:%.1f rake:%.1f s2norm:%.2f>" % (self.Rcalc,self.mu_st,self.mu_dy,self.cohesion,self.rhoeff,self.zref,self.strike,self.dip,self.rake,self.s2norm)
