import argparse
from math import pi
import os
import h5py
import numpy as np
from computeR import *


if not os.path.exists('output'):
    os.makedirs('output')

Rtarg=0.6
mu_st=0.6
mu_dy=0.1
cohesion=0.0
rhoeff=890.
zref=-10e3

from cParamComputeR import cParamComputeR
lstrike = np.load('data/lstrike.npy')
ldip = np.load('data/ldip.npy')
lrake = np.load('data/lrake.npy')
ls2norm = np.load('data/ls2norm.npy')

lmis_rake = np.load('data/lmis_rake.npy')
lmis_slip = np.load('data/lmis_slip.npy')

lmis1 = np.zeros((lstrike.shape[0],3))
lmis2 = np.zeros((ldip.shape[0],3))
lmis3 = np.zeros((lrake.shape[0],3))
lmis4 = np.zeros((ls2norm.shape[0],3))

misfitnames=['misfit rake','misfit slip','sum of misfit']
for i0,lmis in enumerate([lmis_rake,lmis_slip, lmis_rake+lmis_slip]):
   print misfitnames[i0], np.amin(lmis)
   i,j,k,l = np.where(lmis ==np.amin(lmis))
   print lstrike[i],ldip[j],lrake[k],ls2norm[l]
   
   for i,strike in enumerate(lstrike):
      lmis1[i,i0] = np.amin(lmis[i,:,:,:])
   for i,dip in enumerate(ldip):
      lmis2[i,i0] = np.amin(lmis[:,i,:,:])
   for i,rake in enumerate(lrake):
      lmis3[i,i0] = np.amin(lmis[:,:,i,:])
   for i,s2norm in enumerate(ls2norm):
      lmis4[i,i0] = np.amin(lmis[:,:,:,i])

draw_plot=True
if draw_plot:
   import matplotlib.pyplot as plt
   plt.plot(lstrike+30., lmis1[:,0], 'b')
   plt.plot(lstrike+30., lmis1[:,1], 'r')
   plt.plot(lstrike+30., lmis1[:,2], 'k')
   plt.xlabel('strike')
   plt.ylabel('misfit')
   plt.legend(['misfit rake', 'misfit slip','sum of misfit'], loc=2)
   plt.savefig('output/misfitstrike.png', bbox_inches='tight')

   plt.figure()
   plt.plot(ldip, lmis2[:,0], 'b')
   plt.plot(ldip, lmis2[:,1], 'r')
   plt.plot(ldip, lmis2[:,2], 'k')
   plt.xlabel('dip')
   plt.ylabel('misfit')
   plt.legend(['misfit rake', 'misfit slip','sum of misfit'], loc=2)
   plt.savefig('output/misfitdip.png', bbox_inches='tight')

   plt.figure()
   plt.plot(lrake, lmis3[:,0], 'b')
   plt.plot(lrake, lmis3[:,1], 'r')
   plt.plot(lrake, lmis3[:,2], 'k')
   plt.xlabel('rake')
   plt.ylabel('misfit')
   plt.legend(['misfit rake', 'misfit slip','sum of misfit'], loc=2)
   plt.savefig('output/misfitrake.png', bbox_inches='tight')

   plt.figure()
   plt.plot(ls2norm, lmis4[:,0], 'b')
   plt.plot(ls2norm, lmis4[:,1], 'r')
   plt.plot(ls2norm, lmis4[:,2], 'k')
   plt.xlabel('s2norm')
   plt.ylabel('misfit')
   plt.legend(['misfit rake', 'misfit slip','sum of misfit'], loc=2)
   plt.savefig('output/misfits2norm.png', bbox_inches='tight')

