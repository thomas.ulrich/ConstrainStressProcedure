from math import pi, atan, sin, cos
import numpy as np

def computeNormalizedStress(par):
   #most favorable direction (A4, AM2003)
   R = par.Rcalc
   mu_st=par.mu_st
   mu_dy=par.mu_dy
   cohesion=par.cohesion
   s2norm=par.s2norm

   Phi = pi/4.-0.5*atan(par.mu_st)
   s2=sin(2.*Phi)
   c2=cos(2.*Phi)
   strike_rad = par.strike*pi/180.
   dip_rad = par.dip*pi/180.
   rake_rad = par.rake*pi/180.
   alpha = (2.*par.s2norm-1.)/3.
   sigmazz = par.rhoeff*9.8*par.zref
   if True:
      #ECS for effective confining stress
      ECS=abs(sigmazz)
      #ds (delta_sigma) is computed assuming that P=sigzz (A6, Aochi and Madariaga 2003)
      ds =  (mu_dy * ECS + R*(cohesion + (mu_st-mu_dy)*ECS)) / (s2 + mu_dy*(c2+alpha) + R*(mu_st-mu_dy)*(c2+alpha))
      P = ECS - alpha*ds
      sii=-np.array([P+ds,P-ds + 2.*ds*s2norm,P-ds])

      #first rotation: of axis U_t
      phi_xyz=Phi
      c=cos(phi_xyz)
      s=sin(phi_xyz)
      R0 = np.array([[1., 0., 0.], [0., c, -s], [0., s, c]])

      #second rotation of axis U_n
      phi_xyz= rake_rad
      c=cos(phi_xyz);
      s=sin(phi_xyz);
      R1 = np.array([[c, -s, 0.], [s, c, 0.], [0., 0., 1.]])

      #third rotation: of axis U_s
      phi_xyz= dip_rad
      c=cos(phi_xyz)
      s=sin(phi_xyz)
      R2 = np.array([[c, 0., s], [0., 1., 0.], [-s, 0., c]])

      #4th rotation of axis U_z
      phi_xyz=- strike_rad
      c=cos(phi_xyz);
      s=sin(phi_xyz);
      R3 = np.array([[c, -s, 0.], [s, c, 0.], [0., 0., 1.]])

      Stress = np.array([[sii[1], 0., 0.], [0., sii[0], 0.], [0., 0., sii[2]]])
      return np.matmul(R3,np.matmul(R2,np.matmul(R1,np.matmul(R0,np.matmul(Stress,np.matmul(np.transpose(R0),np.matmul(np.transpose(R1),np.matmul(np.transpose(R2),np.transpose(R3)))))))))/sigmazz

def ComputeZcUsUdUn(xyz, connect):
   nElements = np.shape(connect)[0]
   #z of triangle barycenters
   zcenters = (xyz[connect[:,0],2] + xyz[connect[:,1],2] + xyz[connect[:,2],2])/3.
   #print "minimum depth set to 1.5km"
   zcenters[zcenters>-1.5e3] = -1.5e3
   #compute triangle normals
   un = np.cross(xyz[connect[:,1],:]-xyz[connect[:,0],:],xyz[connect[:,2],:]-xyz[connect[:,0],:])
   norm=np.apply_along_axis(np.linalg.norm, 1, un)
   un = un/norm.reshape((nElements,1))
   un=un.T
   
   #compute Strike and dip direction
   us = np.zeros(un.shape)
   us[0,:] = -un[1,:]
   us[1,:] = un[0,:]
   norm=np.apply_along_axis(np.linalg.norm, 0, us)
   us = us/norm
   ud = np.cross(un.T, us.T).T
   return (zcenters,us,ud,un)


def Rcalc(zcenters,us,ud,un, Parameters):
   nElements = np.shape(us)[1]
   #compute Stress
   normStress = computeNormalizedStress(Parameters)
   #print normStress
   #print normStress/normStress[2,2]

   #compute Traction
   Tractions = normStress.dot(un)*zcenters*Parameters.rhoeff*9.8

   #compute Traction components
   Pn0 = np.sum(Tractions*un, axis=0)    
   Ts0 = np.sum(Tractions*us, axis=0)    
   Td0 = np.sum(Tractions*ud, axis=0)

   Rcalc = np.maximum(0,np.sqrt(Ts0**2+Td0**2)+Parameters.mu_dy*Pn0)/(-(Parameters.mu_st-Parameters.mu_dy)*Pn0+Parameters.cohesion)
   RakeCalc = np.arctan2(Td0,-Ts0)*180./3.1415
   Rcalc = Rcalc.reshape((1,nElements))
   RakeCalc = RakeCalc.reshape((1,nElements))
   return Rcalc,RakeCalc
